/* 
 * File:   main.cpp
 * Author: root
 *
 * Created on December 3, 2015, 7:22 PM
 */

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <stdint.h>

using namespace std;

enum EOpCode{
    R_INSTR = 0,
    ADDI    = 8,
    BEQ     = 4,
    BNE     = 5,
    LW      = 35,
    SW      = 43,
    JAL     = 3
};

enum EFunct{
    ADD     = 32,
    AND     = 36,
    OR      = 37,
    SLT     = 42,
    SLL     = 0,
    SRL     = 2,
    SUB     = 34,
    JR      = 8
};

struct SInstruction{
    uint32_t opCode; //Mask first 6 bits
    //Funct je na poslednich 6bitech
    uint32_t funct; //Mask last 6 bits
       
    uint32_t regT, regS, regD, shamt;   //Registry 
    uint32_t immediate;
};

string getInstructionName( uint32_t _opCode, uint32_t _funct ){
    //OpCode instrukce je na prvnich 6bitech
    cout << "op:" << dec << _opCode << " fnc:" << _funct << endl;
    switch( _opCode ){
        case( R_INSTR ):
            switch( _funct ){
                case( ADD ): return "add ";
                case( AND ): return "and ";
                case( OR  ): return "or  ";
                case( SLT ): return "slt ";
                case( SLL ): return "sll ";
                case( SRL ): return "srl ";
                case( SUB ): return "sub ";
                case( JR  ): return "jr  ";
                default:
                    return "UNKNOW FUN";
            }
            break;
        case( ADDI ):   return "addi";
        case( BEQ ):    return "beq ";
        case( BNE ):    return "bne ";
        case( LW ):     return "lw  ";
        case( SW ):     return "sw  ";
        case( JAL ):    return "jal ";
        default:
            return "UNKNOW OP";
    }
}

string num2Bin( uint32_t _num ){
    string bin;
    uint32_t position = 1 << (sizeof(uint32_t)*8-1);
    bin.reserve( sizeof(uint32_t)*8 + 7 );
    for( unsigned int i = 0; i < sizeof(uint32_t)*8; i++ ){
        bin.append( (_num & position)?"1":"0" );
        if( (i+1)%4 == 0 ){
            //bin.append( " " );
        }
        position >>= 1;
    }
    return bin;
}

/*
 "d$R"
 "s$R"
 "t$R"
 */
void appendOperation( const SInstruction &_instr, ofstream &_assFile  ){
    //OpCode instrukce je na prvnich 6bitech
    
    switch( _instr.opCode ){
        case( R_INSTR ):
            switch( _instr.funct ){
                case( ADD ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" + t$R"<<_instr.regT; 
                    break;
                case( AND ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" & t$R"<<_instr.regT;
                    break;
                case( OR  ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" | t$R"<<_instr.regT;
                    break;
                case( SLT ): 
                    _assFile << "\t\tif s$R"<<_instr.regS<<" < t$R"<<_instr.regT<<" d$R"<<_instr.regD << " = 1; else d$R"<<_instr.regD << " = 0";
                    break;
                case( SLL ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" << t$R"<<_instr.regT;
                    break;
                case( SRL ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" >> t$R"<<_instr.regT;
                    break;
                case( SUB ): 
                    _assFile << "\t\td$R"<<_instr.regD<<" = s$R"<<_instr.regS<<" - t$R"<<_instr.regT;
                    break;
                case( JR  ): 
                    _assFile << "\t\tgo to address $sR" << _instr.regS;
                    break;
                default:
                    break;
            }
            break;
        case( ADDI ):   
            _assFile << "\t\tt$R"<<_instr.regT<<" = s$R"<<_instr.regS<<" + "<<(int16_t)_instr.immediate;
            break;
        case( BEQ ):
            _assFile << "\t\tif s$R"<<_instr.regS<<" == t$R"<<_instr.regT<<" go to PC+4+4*"<<(int16_t)_instr.immediate << " else go to PC+4";
            break;
        case( BNE ):
            _assFile << "\t\tif s$R"<<_instr.regS<<" != t$R"<<_instr.regT<<" go to PC+4+4*"<<(int16_t)_instr.immediate << " else go to PC+4";
            break;
        case( LW ):
            _assFile << "\t\t$tR" << _instr.regT << " = MEM[$sR" << _instr.regS << "] + " << (int16_t)_instr.immediate;
            break;
        case( SW ):
            _assFile  << "\t\tMEM[$sR" << _instr.regS << "] + " << (int16_t)_instr.immediate<< " = $tR" << _instr.regT;
            break;
        case( JAL ):
            _assFile << "\t\t\t\t\t$31 = PC + 8; PC = (PC & 0xF0000000) | (" << (int32_t)_instr.immediate << " << 2)";
            break;
        default:
            break;
    }
}
/*
 * 
 */
int main(int argc, char** argv) {
    
    if( argc < 2 ){
        cout << "Use ./bin2ass file2decomp" << endl;
        return 0;
    }
    
    //=== Open file with hex code
    ifstream hexfile;   //Soubor s hex zapisem binarky
    hexfile.open( argv[1], ios::in | ios::binary );
    if( hexfile.fail() ){
        cout << "Error opening file" << endl;
        return 0;
    }
    //istream_iterator<uint32_t> start(hexfile),end;
    
    //=== put hex instructions to vector
    vector<uint32_t> hexInstructions;
    
    string line;
    stringstream ss;
    uint32_t tmp = 0;
    while( getline( hexfile, line  ) ){
        ss << hex << line;
        ss >> tmp;
        //cout << tmp << " " << line << endl;
        hexInstructions.push_back( tmp );
    }
    
    
    cout << "Read " << dec << hexInstructions.size() << " instructions." << endl;
    int index = 0;
    for( vector<uint32_t>::iterator it = hexInstructions.begin(); it != hexInstructions.end(); ++it )
    {
        cout << "Number "<< dec << ++index << " " << hex << *it << endl;
    }

    // Vystupni soubor
    ofstream assemblerFile;
    assemblerFile.open( "assembler.txt", ios::out );
    
    
    for( vector<uint32_t>::iterator it = hexInstructions.begin(); it != hexInstructions.end(); ++it )
    {
        uint32_t instruction = *it;
        string instr;
        
        assemblerFile << num2Bin( instruction ) << " ";
        
        //=== Get instruction
        SInstruction tmpInstr;
        //OpCode instrukce je na prvnich 6bitech
        tmpInstr.opCode = (instruction & 0xFC000000 ) >> 26 ; //Mask first 6 bits
        //Funct je na poslednich 6bitech
        tmpInstr.funct  = instruction & 0x3F; //Mask last 6 bits
        
        cout << hex <<  instruction << " op:" << dec << tmpInstr.opCode << " fnc:" << tmpInstr.funct << " ";
        switch( tmpInstr.opCode ){
            case( R_INSTR ):
                switch( tmpInstr.funct ){
                    case( ADD ): 
                    case( AND ):
                    case( OR  ):
                    case( SLT ):
                    case( SUB ):
                        tmpInstr.regS = ( instruction & 0x03E00000 ) >> 21;
                        tmpInstr.regT = ( instruction & 0x001F0000 ) >> 16;
                        tmpInstr.regD = ( instruction & 0x0000F800 ) >> 11;
                        assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                        assemblerFile << "d$R" << dec << tmpInstr.regD << ", ";
                        assemblerFile << "s$R" << dec << tmpInstr.regS << ", ";
                        assemblerFile << "t$R" << dec << tmpInstr.regT << " ";
                        break;
                    case( SLL ):
                    case( SRL ):
                        tmpInstr.regT = ( instruction & 0x001F0000 ) >> 16;
                        tmpInstr.regD = ( instruction & 0x0000F800 ) >> 11;
                        tmpInstr.shamt = ( instruction & 0x000007C0 ) >> 6;
                        assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                        assemblerFile << "d$R" << dec << tmpInstr.regD << ", ";
                        assemblerFile << "t$R" << dec << tmpInstr.regT << ", ";
                        assemblerFile << tmpInstr.shamt << " ";
                        break;
                    case( JR  ):
                        tmpInstr.regS = ( instruction & 0x03E00000 ) >> 21;
                        assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                        assemblerFile << "s$R" << dec << tmpInstr.regS << " ";
                        break;
                    default:
                        cout << "Unknow instr R" << " ";
                        break;
                }
                break;
            case( ADDI ):
            case( BEQ ): 
            case( BNE ): 
                tmpInstr.regS = ( instruction & 0x03E00000 ) >> 21;
                tmpInstr.regT = ( instruction & 0x001F0000 ) >> 16;
                tmpInstr.immediate = ( instruction & 0x0000FFFF );
                
                assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                assemblerFile << "t$R" << dec << tmpInstr.regT << ", ";
                assemblerFile << "s$R" << dec << tmpInstr.regS << ", "; 
                assemblerFile << (int16_t)tmpInstr.immediate << " ";
                break;
            case( LW ):  
            case( SW ):  
                tmpInstr.regS = ( instruction & 0x03E00000 ) >> 21;
                tmpInstr.regT = ( instruction & 0x001F0000 ) >> 16;
                tmpInstr.immediate = ( instruction & 0x0000FFFF );
                
                assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                assemblerFile << "t$R" << dec << tmpInstr.regT << ", ";
                assemblerFile << (int16_t)tmpInstr.immediate;
                assemblerFile << "(s$R" << dec << tmpInstr.regS << ")" << " "; 
                break;
            case( JAL ): 
                tmpInstr.immediate = ( instruction & 0x03FFFFFF );
                assemblerFile << getInstructionName( tmpInstr.opCode, tmpInstr.funct ) << "\t";
                assemblerFile << tmpInstr.immediate << " ";
                break;
            default:
                cout << "Unknow instruction" << " ";
                break;
        }
        
        appendOperation( tmpInstr, assemblerFile );
        assemblerFile << endl;
        cout << instr << endl;
    }
    assemblerFile.close();
    hexfile.close();
    return 0;
}

